
#ifndef __SMCLASS_controller_H
#define __SMCLASS_controller_H
#include "QStateMachine"
#include "QSignalTransition"
#include "QTimer"
#include "QMetaMethod"
#include "QPointer"
#include "QVariant"


 #include "QFinalState"

 #include "QHistoryState"

#include "QHash"
#include "QEventTransition"

#include "QDebug"


#define In(state) (configuration().contains(state_##state))



class SMClass_controller;
    
     class SCC_UnconditionalTransition : public QAbstractTransition
     {
     public:
         SCC_UnconditionalTransition(QState* s)
             : QAbstractTransition(s) {}
     protected:
         void onTransition(QEvent *) {}
         bool eventTest(QEvent *) { return true; }
     };
    
      #define ARG_FROM_VAR(I) \
          (acount > I \
                ? QGenericArgument(((QStateMachine::SignalEvent*)event)->arguments()[I].typeName(),((QStateMachine::SignalEvent*)event)->arguments()[I].data()) \
                : QGenericArgument())
                

      class SCC_EventSender : public QTimer
      {
          Q_OBJECT
          private:
            QStateMachine* machine;
            QStateMachine::SignalEvent* event;
          public:
          SCC_EventSender(QStateMachine* m=NULL, int delay=0, QStateMachine::SignalEvent* e=NULL) : QTimer(m), machine(m), event(e)
          {
            setInterval(delay);
            setSingleShot(true);
            connect(this,SIGNAL(timeout()),this,SLOT(send()));
            start();
          }
          public Q_SLOTS:
            void cancel() { stop(); deleteLater(); }
            void send() { 
              QVariantList args = event->arguments();
              int acount = args.count();
              event->sender()->metaObject()->method(event->signalIndex()).invoke(event->sender(),
                          ARG_FROM_VAR(0),ARG_FROM_VAR(1),ARG_FROM_VAR(2),ARG_FROM_VAR(3),ARG_FROM_VAR(4),
                          ARG_FROM_VAR(5),ARG_FROM_VAR(6),ARG_FROM_VAR(7),ARG_FROM_VAR(8),ARG_FROM_VAR(9));
              deleteLater();
            }
      };
    
namespace {
    
        /*
            <transition event="login_complete(bool)" cond="_event.data[0].toBool()" target="loggedIn"/>
        */

        class Transition_T396567041980 : public QSignalTransition
        {
            SMClass_controller* stateMachine;
            public:
                Transition_T396567041980(QState* parent)
                    : QSignalTransition(parent->machine(),SIGNAL(event_login_complete(bool)),parent)
                      ,stateMachine((SMClass_controller*)parent->machine())
                {
                }

            protected:
                bool eventTest(QEvent* e);
        };

    
        /*
            <transition event="login_complete(bool)" cond="!_event.data[0].toBool()" target="loginError"/>
        */

        class Transition_T396567042030 : public QSignalTransition
        {
            SMClass_controller* stateMachine;
            public:
                Transition_T396567042030(QState* parent)
                    : QSignalTransition(parent->machine(),SIGNAL(event_login_complete(bool)),parent)
                      ,stateMachine((SMClass_controller*)parent->machine())
                {
                }

            protected:
                bool eventTest(QEvent* e);
        };

    
        /*
            <transition target="gui_active" cond="!In(idle)"/>
        */

        class Transition_T396567043610 : public QSignalTransition
        {
            SMClass_controller* stateMachine;
            public:
                Transition_T396567043610(QState* parent)
                    : QSignalTransition(parent->machine(),SIGNAL(event_),parent)
                      ,stateMachine((SMClass_controller*)parent->machine())
                {
                }

            protected:
                bool eventTest(QEvent* e);
        };

    
        /*
            <transition target="gui_idle" cond="In(idle)"/>
        */

        class Transition_T396567043690 : public QSignalTransition
        {
            SMClass_controller* stateMachine;
            public:
                Transition_T396567043690(QState* parent)
                    : QSignalTransition(parent->machine(),SIGNAL(event_),parent)
                      ,stateMachine((SMClass_controller*)parent->machine())
                {
                }

            protected:
                bool eventTest(QEvent* e);
        };

    
};
    
class SMClass_controller : public QStateMachine
{
    Q_OBJECT

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="username" Qt:type="QString"/>
        */

    Q_PROPERTY(QString username READ get_username WRITE set_username NOTIFY username_changed)

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="password" Qt:type="QString"/>
        */

    Q_PROPERTY(QString password READ get_password WRITE set_password NOTIFY password_changed)

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginTimeout" Qt:type="int" expr="10000"/>
        */

    Q_PROPERTY(int loginTimeout READ get_loginTimeout WRITE set_loginTimeout NOTIFY loginTimeout_changed)

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="firstTime" Qt:type="bool" expr="true"/>
        */

    Q_PROPERTY(bool firstTime READ get_firstTime WRITE set_firstTime NOTIFY firstTime_changed)

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginButton" Qt:type="QObject*" expr="NULL"/>
        */

    Q_PROPERTY(QObject* loginButton READ get_loginButton WRITE set_loginButton NOTIFY loginButton_changed)

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="logoutButton" Qt:type="QObject*" expr="NULL"/>
        */

    Q_PROPERTY(QObject* logoutButton READ get_logoutButton WRITE set_logoutButton NOTIFY logoutButton_changed)

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="cancelButton" Qt:type="QObject*" expr="NULL"/>
        */

    Q_PROPERTY(QObject* cancelButton READ get_cancelButton WRITE set_cancelButton NOTIFY cancelButton_changed)


    public:
        SMClass_controller(QObject* o = NULL)
            : QStateMachine(o)
              {

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginTimeout" Qt:type="int" expr="10000"/>
        */

            _data.loginTimeout = 10000;
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="firstTime" Qt:type="bool" expr="true"/>
        */

            _data.firstTime = true;
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginButton" Qt:type="QObject*" expr="NULL"/>
        */

            _data.loginButton = NULL;
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="logoutButton" Qt:type="QObject*" expr="NULL"/>
        */

            _data.logoutButton = NULL;
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="cancelButton" Qt:type="QObject*" expr="NULL"/>
        */

            _data.cancelButton = NULL;
    
              }
    
        /*
            <parallel id="root"/>
        */

        QState* state_root;
        /*
            <state id="business_logic"/>
        */

        QState* state_business_logic;
        /*
            <initial/>
        */

        QState* state_T39656704460;
        /*
            <state id="idle"/>
        */

        QState* state_idle;
        /*
            <state id="trying_to_login"/>
        */

        QState* state_trying_to_login;
        /*
            <state id="error" initial="loginError"/>
        */

        QState* state_error;
        /*
            <state id="cancelled"/>
        */

        QState* state_cancelled;
        /*
            <state id="loginError"/>
        */

        QState* state_loginError;
        /*
            <state id="loginTimeout"/>
        */

        QState* state_loginTimeout;
        /*
            <state id="loggedIn"/>
        */

        QState* state_loggedIn;
        /*
            <state id="active"/>
        */

        QState* state_active;
        /*
            <state id="gui" initial="hist"/>
        */

        QState* state_gui;
        /*
            <state id="gui_idle"/>
        */

        QState* state_gui_idle;
        /*
            <state id="gui_active"/>
        */

        QState* state_gui_active;
        /*
            <final id="exit"/>
        */

        QFinalState* state_exit;
    
        /*
            <history id="hist" type="deep"/>
        */

        QHistoryState* state_hist;
    
        /*
            <transition event="login_complete(bool)" cond="_event.data[0].toBool()" target="loggedIn"/>
        */

        inline bool testCondition_T396567041980()
        {
            return _event.data[0].toBool();
        }
    
        /*
            <transition event="login_complete(bool)" cond="!_event.data[0].toBool()" target="loginError"/>
        */

        inline bool testCondition_T396567042030()
        {
            return !_event.data[0].toBool();
        }
    
        /*
            <transition target="gui_active" cond="!In(idle)"/>
        */

        inline bool testCondition_T396567043610()
        {
            return !In(idle);
        }
    
        /*
            <transition target="gui_idle" cond="In(idle)"/>
        */

        inline bool testCondition_T396567043690()
        {
            return In(idle);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="username" Qt:type="QString"/>
        */
        QString get_username() const
        {
            return _data.username;
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="password" Qt:type="QString"/>
        */
        QString get_password() const
        {
            return _data.password;
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginTimeout" Qt:type="int" expr="10000"/>
        */
        int get_loginTimeout() const
        {
            return _data.loginTimeout;
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="firstTime" Qt:type="bool" expr="true"/>
        */
        bool get_firstTime() const
        {
            return _data.firstTime;
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginButton" Qt:type="QObject*" expr="NULL"/>
        */
        QObject* get_loginButton() const
        {
            return _data.loginButton;
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="logoutButton" Qt:type="QObject*" expr="NULL"/>
        */
        QObject* get_logoutButton() const
        {
            return _data.logoutButton;
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="cancelButton" Qt:type="QObject*" expr="NULL"/>
        */
        QObject* get_cancelButton() const
        {
            return _data.cancelButton;
        }
    
    protected:
        struct {
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="username" Qt:type="QString"/>
        */
              QString username;
            
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="password" Qt:type="QString"/>
        */
              QString password;
            
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginTimeout" Qt:type="int" expr="10000"/>
        */
              int loginTimeout;
            
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="firstTime" Qt:type="bool" expr="true"/>
        */
              bool firstTime;
            
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginButton" Qt:type="QObject*" expr="NULL"/>
        */
              QObject* loginButton;
            
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="logoutButton" Qt:type="QObject*" expr="NULL"/>
        */
              QObject* logoutButton;
            
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="cancelButton" Qt:type="QObject*" expr="NULL"/>
        */
              QObject* cancelButton;
            
        } _data;
           
        struct {
          QString name;
          QVariantList data;
        } _event;
        QString _name;
             
    public Q_SLOTS:

        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="username" Qt:type="QString"/>
        */

        void set_username(QString const & value)
        {
            _data.username = value;
            emit username_changed(value);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="password" Qt:type="QString"/>
        */

        void set_password(QString const & value)
        {
            _data.password = value;
            emit password_changed(value);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginTimeout" Qt:type="int" expr="10000"/>
        */

        void set_loginTimeout(int const & value)
        {
            _data.loginTimeout = value;
            emit loginTimeout_changed(value);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="firstTime" Qt:type="bool" expr="true"/>
        */

        void set_firstTime(bool const & value)
        {
            _data.firstTime = value;
            emit firstTime_changed(value);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginButton" Qt:type="QObject*" expr="NULL"/>
        */

        void set_loginButton(QObject* const & value)
        {
            _data.loginButton = value;
            emit loginButton_changed(value);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="logoutButton" Qt:type="QObject*" expr="NULL"/>
        */

        void set_logoutButton(QObject* const & value)
        {
            _data.logoutButton = value;
            emit logoutButton_changed(value);
        }
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="cancelButton" Qt:type="QObject*" expr="NULL"/>
        */

        void set_cancelButton(QObject* const & value)
        {
            _data.cancelButton = value;
            emit cancelButton_changed(value);
        }
    
    private Q_SLOTS:
#ifndef QT_NO_PROPERTIES
        void assignProperties()
        {
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.cancelButton" property="enabled" value="false"/>
        */
        state_idle->assignProperty(_data.cancelButton,"enabled",QVariant(false));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.cancelButton" property="enabled" value="true"/>
        */
        state_trying_to_login->assignProperty(_data.cancelButton,"enabled",QVariant(true));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.cancelButton" property="enabled" value="false"/>
        */
        state_error->assignProperty(_data.cancelButton,"enabled",QVariant(false));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.cancelButton" property="enabled" value="false"/>
        */
        state_active->assignProperty(_data.cancelButton,"enabled",QVariant(false));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.loginButton" property="enabled" value="true"/>
        */
        state_gui_idle->assignProperty(_data.loginButton,"enabled",QVariant(true));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.logoutButton" property="enabled" value="false"/>
        */
        state_gui_idle->assignProperty(_data.logoutButton,"enabled",QVariant(false));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.loginButton" property="enabled" value="false"/>
        */
        state_gui_active->assignProperty(_data.loginButton,"enabled",QVariant(false));
        
        /*
            <Qt:property xmlns:Qt="http://www.qtsoftware.com scxml-ext" object="_data.logoutButton" property="enabled" value="true"/>
        */
        state_gui_active->assignProperty(_data.logoutButton,"enabled",QVariant(true));
        
#endif
        }
     
        /*
            <initial/>
        */

        /*
            <transition target="idle"/>
        */

        void exec_T39656704480()
        {
            
        
        /*
            <if cond="_data.firstTime"/>
        */

              if (_data.firstTime) {
  
            
        /*
            <send event="notify_welcome()"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_notify_welcome()")),QVariantList()));

          
        /*
            <if cond="!_data.username.isEmpty()"/>
        */

              if (!_data.username.isEmpty()) {
  
            
        /*
            <send event="notify_hello(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_notify_hello(QString)")),QVariantList()<< QVariant(_data.username)
                      ));

          
              }

          
        /*
            <assign dataid="firstTime" expr="false"/>
        */

                set_firstTime(false);
    
        
              }

      
        }
    
        /*
            <state id="idle"/>
        */

        /*
            <onentry/>
        */

        void exec_T39656704780()
        {
            
	
        /*
            <log level="0" label="Info" expr=""Now in Idle""/>
        */

                QDebug((QtMsgType)0) << "Info" << "Now in Idle";

        
        /*
            <log level="2" label="PowerManagement" expr=""Acquiring Resource USB""/>
        */

                QDebug((QtMsgType)2) << "PowerManagement" << "Acquiring Resource USB";

        
        /*
            <send event="acquire_res(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_acquire_res(QString)")),QVariantList()<< QVariant("USB")
                      ));

	
        /*
            <raise event="in_idle"/>
        */
postEvent(
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_in_idle()")),QVariantList()),QStateMachine::HighPriority);

      
        }
    
        /*
            <state id="idle"/>
        */

        /*
            <onexit/>
        */

        void exec_T396567041010()
        {
            
        /*
            <log level="0" label="Info" expr=""Now not in Idle""/>
        */

                QDebug((QtMsgType)0) << "Info" << "Now not in Idle";

        /*
            <raise event="not_in_idle"/>
        */
postEvent(
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_not_in_idle()")),QVariantList()),QStateMachine::HighPriority);

        }
    
        /*
            <state id="trying_to_login"/>
        */

        /*
            <onentry/>
        */

        void exec_T396567041350()
        {
            
        
        /*
            <log level="0" label="Info" expr=""Trying to log in""/>
        */

                QDebug((QtMsgType)0) << "Info" << "Trying to log in";

        
        /*
            <send event="login_action(QString,QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_login_action(QString,QString)")),QVariantList()<< QVariant(_data.username)
                      << QVariant(_data.password)
                      ));

        
        /*
            <log level="2" label="PowerManagement" expr=""Releasing Resource USB""/>
        */

                QDebug((QtMsgType)2) << "PowerManagement" << "Releasing Resource USB";

        
        /*
            <send event="release_res(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_release_res(QString)")),QVariantList()<< QVariant("USB")
                      ));

        
        /*
            <log level="2" label="PowerManagement" expr=""Acquiring Resource Network""/>
        */

                QDebug((QtMsgType)2) << "PowerManagement" << "Acquiring Resource Network";

        
        /*
            <send event="acquire_res(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_acquire_res(QString)")),QVariantList()<< QVariant("Network")
                      ));

        
        /*
            <send event="login_action(QString,QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_login_action(QString,QString)")),QVariantList()<< QVariant(_data.username)
                      << QVariant(_data.password)
                      ));

        
        /*
            <send id="cancel_login_timeout" event="login_timeout()" delay="_data.loginTimeout"/>
        */

                 _eventSenders["cancel_login_timeout"] = new SCC_EventSender(this,_data.loginTimeout,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_login_timeout()")),QVariantList()));

      
        }
    
        /*
            <state id="trying_to_login"/>
        */

        /*
            <onexit/>
        */

        void exec_T396567041920()
        {
            
        
        /*
            <cancel id="cancel_login_timeout"/>
        */

                  {
                    QPointer<SCC_EventSender> es = _eventSenders["cancel_login_timeout"];
                    if (es)
                      es->cancel();
                  }

      
        }
    
        /*
            <state id="error" initial="loginError"/>
        */

        /*
            <onentry/>
        */

        void exec_T396567042220()
        {
            
	
        /*
            <log level="2" label="PowerManagement" expr=""Releasing Resource USB""/>
        */

                QDebug((QtMsgType)2) << "PowerManagement" << "Releasing Resource USB";

        
        /*
            <send event="release_res(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_release_res(QString)")),QVariantList()<< QVariant("USB")
                      ));

	
        /*
            <log level="2" label="PowerManagement" expr=""Releasing Resource Network""/>
        */

                QDebug((QtMsgType)2) << "PowerManagement" << "Releasing Resource Network";

        
        /*
            <send event="release_res(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_release_res(QString)")),QVariantList()<< QVariant("Network")
                      ));

        
        /*
            <raise event="error.login()"/>
        */
postEvent(
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_error__login()")),QVariantList()),QStateMachine::HighPriority);

      
        }
    
        /*
            <state id="cancelled"/>
        */

        /*
            <onentry/>
        */

        void exec_T396567042550()
        {
            
        
        /*
            <send event="notify_cancel()"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_notify_cancel()")),QVariantList()));

      
        }
    
        /*
            <state id="loginError"/>
        */

        /*
            <onentry/>
        */

        void exec_T396567042650()
        {
            
          
        /*
            <log label="Error" level="1" expr="_data.username + ' ' + _data.password"/>
        */

                QDebug((QtMsgType)1) << "Error" << _data.username + ' ' + _data.password;

          
        /*
            <send event="notify_error()"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_notify_error()")),QVariantList()));

        
        }
    
        /*
            <state id="loginTimeout"/>
        */

        /*
            <onentry/>
        */

        void exec_T396567042800()
        {
            
          
        /*
            <log label="Error" level="1" expr="_data.username + ' ' + _data.password"/>
        */

                QDebug((QtMsgType)1) << "Error" << _data.username + ' ' + _data.password;

          
        /*
            <send event="notify_timeout()"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_notify_timeout()")),QVariantList()));

        
        }
    
        /*
            <state id="loggedIn"/>
        */

        /*
            <onentry/>
        */

        void exec_T396567042960()
        {
            
	
        /*
            <log level="2" label="PowerManagement" expr=""Releasing Resource USB""/>
        */

                QDebug((QtMsgType)2) << "PowerManagement" << "Releasing Resource USB";

        
        /*
            <send event="release_res(QString)"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_release_res(QString)")),QVariantList()<< QVariant("USB")
                      ));

        
        /*
            <send event="notify_loggedIn()"/>
        */
new SCC_EventSender(this,0,
                            
                            new QStateMachine::SignalEvent(this,metaObject()->indexOfSignal(QMetaObject::normalizedSignature("event_notify_loggedIn()")),QVariantList()));

      
        }
    
   Q_SIGNALS:
        void event_notify_welcome();
        void event_notify_hello(QString);
        void event_acquire_res(QString);
        void event_in_idle();
        void event_not_in_idle();
        void event_intent_login();
        void event_cancel_login();
        void event_login_timeout();
        void event_login_action(QString,QString);
        void event_release_res(QString);
        void event_login_complete(bool);
        void event_intent_continue();
        void event_error__login();
        void event_notify_cancel();
        void event_notify_error();
        void event_notify_timeout();
        void event_notify_loggedIn();
        void event_intent_logout();
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="username" Qt:type="QString"/>
        */

        void username_changed(QString const &);
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="password" Qt:type="QString"/>
        */

        void password_changed(QString const &);
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginTimeout" Qt:type="int" expr="10000"/>
        */

        void loginTimeout_changed(int const &);
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="firstTime" Qt:type="bool" expr="true"/>
        */

        void firstTime_changed(bool const &);
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="loginButton" Qt:type="QObject*" expr="NULL"/>
        */

        void loginButton_changed(QObject* const &);
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="logoutButton" Qt:type="QObject*" expr="NULL"/>
        */

        void logoutButton_changed(QObject* const &);
    
        /*
            <data xmlns:Qt="http://www.qtsoftware.com scxml-ext" id="cancelButton" Qt:type="QObject*" expr="NULL"/>
        */

        void cancelButton_changed(QObject* const &);
    
    protected:
    virtual void beginSelectTransitions(QEvent *event)
    {
        if (event && !event->type() == QEvent::None) {
          switch (event->type()) {
            case QEvent::StateMachineSignal: {
              QStateMachine::SignalEvent* e = (QStateMachine::SignalEvent*)event;
              _event.data = e->arguments();
              _event.name = e->sender()->metaObject()->method(e->signalIndex()).signature();
              if (e->sender() == this)
                _event.name = _event.name.mid(6);
            } break;
            default:
            break;
          }
        } else {
          _event.name = "";
          _event.data.clear();
        }
         
        assignProperties();
    }

    private:
        QHash<QString,QPointer<SCC_EventSender> > _eventSenders;

    protected:
    public:
        void setupStateMachine()
        {
            _name = "controller";
            setObjectName(_name);
            
        /*
            <parallel id="root"/>
        */
            state_root = new QState(this);
            state_root->setObjectName("root");this->setInitialState(state_root);
            state_root->setChildMode(ParallelStates);
            
        /*
            <state id="business_logic"/>
        */
            state_business_logic = new QState(state_root);
            state_business_logic->setObjectName("business_logic");
        /*
            <initial/>
        */
            state_T39656704460 = new QState(state_business_logic);
            state_business_logic->setInitialState(state_T39656704460);
            
        /*
            <state id="idle"/>
        */
            state_idle = new QState(state_business_logic);
            state_idle->setObjectName("idle");
        /*
            <state id="trying_to_login"/>
        */
            state_trying_to_login = new QState(state_business_logic);
            state_trying_to_login->setObjectName("trying_to_login");
        /*
            <state id="error" initial="loginError"/>
        */
            state_error = new QState(state_business_logic);
            state_error->setObjectName("error");
        /*
            <state id="cancelled"/>
        */
            state_cancelled = new QState(state_error);
            state_cancelled->setObjectName("cancelled");
        /*
            <state id="loginError"/>
        */
            state_loginError = new QState(state_error);
            state_loginError->setObjectName("loginError");state_error->setInitialState(state_loginError);
            
        /*
            <state id="loginTimeout"/>
        */
            state_loginTimeout = new QState(state_error);
            state_loginTimeout->setObjectName("loginTimeout");
        /*
            <state id="loggedIn"/>
        */
            state_loggedIn = new QState(state_business_logic);
            state_loggedIn->setObjectName("loggedIn");
        /*
            <state id="active"/>
        */
            state_active = new QState(state_business_logic);
            state_active->setObjectName("active");
        /*
            <final id="exit"/>
        */
            state_exit = new QFinalState(state_business_logic);
            state_exit->setObjectName("exit");
        /*
            <state id="gui" initial="hist"/>
        */
            state_gui = new QState(state_root);
            state_gui->setObjectName("gui");
        /*
            <history id="hist" type="deep"/>
        */
            state_hist = new QHistoryState(state_gui);
            state_hist->setObjectName("hist");state_gui->setInitialState(state_hist);
            state_hist->setHistoryType(QHistoryState::DeepHistory);
            
        /*
            <state id="gui_idle"/>
        */
            state_gui_idle = new QState(state_gui);
            state_gui_idle->setObjectName("gui_idle");
        /*
            <state id="gui_active"/>
        */
            state_gui_active = new QState(state_gui);
            state_gui_active->setObjectName("gui_active");
            QAbstractTransition* transition;
        /*
            <transition target="idle"/>
        */

            transition = new SCC_UnconditionalTransition(state_T39656704460);
            connect(transition,SIGNAL(triggered()),this,SLOT(exec_T39656704480()));
            transition->setTargetState(state_idle);
        /*
            <transition target="trying_to_login" event="intent_login()"/>
        */

            transition = new QSignalTransition(this,SIGNAL(event_intent_login()),state_idle);
            transition->setTargetState(state_trying_to_login);
        /*
            <transition event="cancel_login()" target="idle"/>
        */

            transition = new QSignalTransition(this,SIGNAL(event_cancel_login()),state_trying_to_login);
            transition->setTargetState(state_idle);
        /*
            <transition event="login_timeout()" target="loginTimeout"/>
        */

            transition = new QSignalTransition(this,SIGNAL(event_login_timeout()),state_trying_to_login);
            transition->setTargetState(state_loginTimeout);
        /*
            <transition event="login_complete(bool)" cond="_event.data[0].toBool()" target="loggedIn"/>
        */

            transition = new Transition_T396567041980(state_trying_to_login);
            transition->setTargetState(state_loggedIn);
        /*
            <transition event="login_complete(bool)" cond="!_event.data[0].toBool()" target="loginError"/>
        */

            transition = new Transition_T396567042030(state_trying_to_login);
            transition->setTargetState(state_loginError);
        /*
            <transition event="intent_continue()" target="idle"/>
        */

            transition = new QSignalTransition(this,SIGNAL(event_intent_continue()),state_error);
            transition->setTargetState(state_idle);
        /*
            <transition event="intent_continue()" target="active"/>
        */

            transition = new QSignalTransition(this,SIGNAL(event_intent_continue()),state_loggedIn);
            transition->setTargetState(state_active);
        /*
            <transition event="intent_logout()" target="idle"/>
        */

            transition = new QSignalTransition(this,SIGNAL(event_intent_logout()),state_active);
            transition->setTargetState(state_idle);
        /*
            <transition target="gui_active" cond="!In(idle)"/>
        */

            transition = new Transition_T396567043610(state_gui_idle);
            transition->setTargetState(state_gui_active);
        /*
            <transition target="gui_idle" cond="In(idle)"/>
        */

            transition = new Transition_T396567043690(state_gui_active);
            transition->setTargetState(state_gui_idle);state_hist->setDefaultState(state_gui_idle);
                  
            connect(state_idle, SIGNAL(entered()),this,SLOT(exec_T39656704780()));
            connect(state_idle, SIGNAL(exited()),this,SLOT(exec_T396567041010()));
            connect(state_trying_to_login, SIGNAL(entered()),this,SLOT(exec_T396567041350()));
            connect(state_trying_to_login, SIGNAL(exited()),this,SLOT(exec_T396567041920()));
            connect(state_error, SIGNAL(entered()),this,SLOT(exec_T396567042220()));
            connect(state_cancelled, SIGNAL(entered()),this,SLOT(exec_T396567042550()));
            connect(state_loginError, SIGNAL(entered()),this,SLOT(exec_T396567042650()));
            connect(state_loginTimeout, SIGNAL(entered()),this,SLOT(exec_T396567042800()));
            connect(state_loggedIn, SIGNAL(entered()),this,SLOT(exec_T396567042960()));
        }
};
    
        /*
            <transition event="login_complete(bool)" cond="_event.data[0].toBool()" target="loggedIn"/>
        */

        bool Transition_T396567041980::eventTest(QEvent* e)
        {
            return QSignalTransition::eventTest(e)
            && stateMachine->testCondition_T396567041980();
        }
    
        /*
            <transition event="login_complete(bool)" cond="!_event.data[0].toBool()" target="loginError"/>
        */

        bool Transition_T396567042030::eventTest(QEvent* e)
        {
            return QSignalTransition::eventTest(e)
            && stateMachine->testCondition_T396567042030();
        }
    
        /*
            <transition target="gui_active" cond="!In(idle)"/>
        */

        bool Transition_T396567043610::eventTest(QEvent* e)
        {
            return (*e).type() != QEvent::None&& stateMachine->testCondition_T396567043610();
        }
    
        /*
            <transition target="gui_idle" cond="In(idle)"/>
        */

        bool Transition_T396567043690::eventTest(QEvent* e)
        {
            return (*e).type() != QEvent::None&& stateMachine->testCondition_T396567043690();
        }
    
#endif
    
